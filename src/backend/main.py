from flask import Flask
from sys import argv
from config import read_from_file

app = Flask(__name__)

@app.route("/")
def index():
    return "none"

if __name__ == "__main__":
    config = read_from_file(argv[1])
    app.run(debug=True)
    exit(0)