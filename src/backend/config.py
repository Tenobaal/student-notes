class Config(dict):
    def __hash__(self):
        return id(self)

    def _immutable(self, *args, **kws):
        raise TypeError('object is immutable')

    __setitem__ = _immutable
    __delitem__ = _immutable
    clear       = _immutable
    update      = _immutable
    setdefault  = _immutable
    pop         = _immutable
    popitem     = _immutable

def __parse(src):
    out = dict()
    current_section = None
    for l in src:
        l = l.strip()
        if l.startswith("[") and l.endswith("]"):
            current_section = l[1:-1]
            out[current_section] = dict()
        elif "=" in l:
            k, v = l.split("=", 1)
            out[current_section][k] = v
        else:
            raise Exception("faulty config file")
    return Config(out)

def read_from_file(filename):
    f = open(filename, "r")
    val = f.readlines()
    f.close()
    return __parse(val)
