phony: all run venv requirements clean

all: run

# run options
APP_LOCATION = src/backend/main.py
CONFIG_LOCATION = initial-config.ini
VENV_PATH = .venv

run: venv
	source $(VENV_PATH)/bin/activate; \
	python $(APP_LOCATION) $(CONFIG_LOCATION)

$(VENV_PATH):
	virtualenv $(VENV_PATH)

venv: $(VENV_PATH)

requirements: venv
	source $(VENV_PATH)/bin/activate; \
	pip install -r requirements.txt

clean:
	rm -rf $(VENV_PATH)